module.exports = function () {
    const Hapi=require('hapi');
// Create a server with a host and port
    const server=Hapi.server({
        host:'localhost',
        port:8000
    });
    server.route({
        method: 'GET',
        path: '/',
        handler: function (request, h) {
            return 'Welcome to the index';
        }
    });
    // Add the route
    server.route({
        method: 'GET',
        path: '/hello',
        handler: function (request, h) {
            return 'hello world';
        }
    });


    server.route({
        method : 'GET',
        path : '/get',
        handler : function (request, reply) {
     
            return { greeting: 'hello ' + request.query.name };
        },

    });


    return server
    
};
